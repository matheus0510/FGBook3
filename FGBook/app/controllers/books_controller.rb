class BooksController < ApplicationController
	before_action :set_book, only: [:show]
  before_action :set_users_book, only: [:edit, :update, :destroy]
	before_action :require_authentication, only: [:show, :edit, :create, :update, :destroy]

  def index
    @books = Book.all
  end

  def show
  end

  def new
    @book = Book.new
  end

  def edit
  end

  def create
    @book = Book.new(book_params)

      if @book.save
        redirect_to @book, notice: t('flash.notice.book_created')
      else
        render :new
      end
  end

  def update
      if @book.update(book_params)
        redirect_to @book, notice: t('flash.notice.book_updated')
      else
        render :edit 
      end
  end

  def destroy
    @book.destroy
    redirect_to books_url  
  end

  private
    def set_book
      @book = Book.find(params[:id])
    end

    def book_params
      params.require(:book).permit(:title, :author, :price, :telephone, :description)
    end
		
		def set_users_books
			@book = current_user.books.find(params[:id])
		end
end
